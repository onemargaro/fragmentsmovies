package com.onemargaro.android.moviesfragments.tasks;

import android.content.Context;
import android.os.AsyncTask;

import com.onemargaro.android.moviesfragments.models.Video;
import com.onemargaro.android.moviesfragments.util.MovieJsonUtils;
import com.onemargaro.android.moviesfragments.util.NetworkUtils;

import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Margaro on 19/10/17.
 */

public class VideoTask extends AsyncTask<String, Void, ArrayList<Video>> {
    private Context context;
    private VideoTaskAsync videoMainThread;


    public VideoTask(VideoTaskAsync videoMainThread, Context context){
        this.context = context;
        this.videoMainThread = videoMainThread;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(ArrayList<Video> videos) {
        this.videoMainThread.videoFinishedTask(videos);
    }

    @Override
    protected ArrayList<Video> doInBackground(String... params) {
        if(params.length==0){
            return null;
        }

        String urlStr = params[0];
        URL videosUrl = NetworkUtils.buildUrl(urlStr);
        System.out.println(videosUrl.toString());

        try{
            String jsonVideosResponse = NetworkUtils
                    .getResponseFromHttpUrl(videosUrl);


            ArrayList<Video> simpleJsonVideos = MovieJsonUtils.getSimpleVideosStringsFromJson(context, jsonVideosResponse);

            return simpleJsonVideos;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
