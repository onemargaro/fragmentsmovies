package com.onemargaro.android.moviesfragments.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.onemargaro.android.moviesfragments.R;
import com.onemargaro.android.moviesfragments.models.Video;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Margaro on 19/10/17.
 */

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.VideoViewHolder>{

    @Override
    public VideoAdapter.VideoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutId = R.layout.movie_video;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;
        View view = inflater.inflate(layoutId, parent, shouldAttachToParentImmediately);
        return new VideoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(VideoAdapter.VideoViewHolder holder, int position) {
        Video v = VideosData.get(position);
        holder.tv_video_title.setText(v.getName());
    }

    @Override
    public int getItemCount() {
        if (null == VideosData) return 0;
        return VideosData.size();
    }

    public class VideoViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener{
        @BindView(R.id.tv_video_title)
        TextView tv_video_title;

        public VideoViewHolder(View itemView){
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int adapterPosition = getAdapterPosition();
            Video v = VideosData.get(adapterPosition);
            clickHandler.videoClick(v);
        }
    }

    public interface VideoAdapterOnClickHandler{
        void videoClick(Video video);
    }

    public final VideoAdapterOnClickHandler clickHandler;
    public List<Video> VideosData;

    public VideoAdapter(VideoAdapterOnClickHandler clickHandler){
        this.clickHandler = clickHandler;
    }

    public void setVideos(List VideosData){
        this.VideosData = VideosData;
        notifyDataSetChanged();
    }

    public ArrayList getVideos(){
        return (ArrayList) VideosData;
    }

}
