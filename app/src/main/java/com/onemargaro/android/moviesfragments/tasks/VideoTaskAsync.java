package com.onemargaro.android.moviesfragments.tasks;


import com.onemargaro.android.moviesfragments.models.Video;

import java.util.ArrayList;

/**
 * Created by Margaro on 19/10/17.
 */

public interface VideoTaskAsync {
    void videoFinishedTask(ArrayList<Video> video);
}
