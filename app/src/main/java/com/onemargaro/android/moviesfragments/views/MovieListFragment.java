package com.onemargaro.android.moviesfragments.views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.onemargaro.android.moviesfragments.R;
import com.onemargaro.android.moviesfragments.adapters.MovieAdapter;
import com.onemargaro.android.moviesfragments.models.Movie;
import com.onemargaro.android.moviesfragments.tasks.MovieTask;
import com.onemargaro.android.moviesfragments.tasks.MovieTaskAsync;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MovieListFragment extends Fragment implements MovieAdapter.MovieAdapterOnClickHandler, MovieTaskAsync {

    @BindView(R.id.rv_movies) RecyclerView mRecyclerView;
    @BindView(R.id.tv_error_message_display) TextView mErrorMessageDisplay;
    @BindView(R.id.pb_loading_indicator) ProgressBar mLoadingIndicator;

    private MovieAdapter mMovieAdapter;
    private GridLayoutManager gridLayoutManager;
    public  onSelectedMovieListener movieListener;

    private String typelist;

    public static MovieListFragment newInstance(Bundle args) {
        MovieListFragment mlf = new MovieListFragment();
        if (args != null)
            mlf.setArguments(args);
        return mlf;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null ){
            if (getArguments().containsKey("typelist")){
                this.typelist = getArguments().getString("typelist");
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final Activity activity = getActivity();
        View rootView = inflater.inflate(R.layout.fragment_movie_list, container, false);
        ButterKnife.bind(this, rootView);
        int orientation = getResources().getConfiguration().orientation;
        if(orientation == Configuration.ORIENTATION_LANDSCAPE)
            gridLayoutManager = new GridLayoutManager(activity,3);
        else
            gridLayoutManager = new GridLayoutManager(activity,2);

        mRecyclerView.setLayoutManager(gridLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        mMovieAdapter = new MovieAdapter(this);
        mRecyclerView.setAdapter(mMovieAdapter);

        if(savedInstanceState == null || !savedInstanceState.containsKey("movies")){
            loadMovieData(this.typelist);
        }else{
            ArrayList<Movie> temp = savedInstanceState.getParcelableArrayList("movies");
            if(temp != null && !temp.isEmpty()){
                mMovieAdapter.setMovies(temp);
            }else{
                loadMovieData(this.typelist);
            }
        }
        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("movies", mMovieAdapter.getMovies());
    }

    private void showMovieDataView(){
        mErrorMessageDisplay.setVisibility(View.INVISIBLE);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    private void showErrorMessage(){
        mErrorMessageDisplay.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.INVISIBLE);
    }

    private void loadMovieData(String type){
        if (type != null){
            new MovieTask(this, getActivity(), mLoadingIndicator).execute(type);
        }else{
            new MovieTask(this, getActivity(), mLoadingIndicator).execute("popular");
        }
    }

    @Override
    public void onClick(Movie movieDetail) {
        this.movieListener.onSelectedMovie(movieDetail);
        /*
        Context context = getActivity();
        Intent intentToStartDetailActivity = new Intent(context, MovieDetailActivity.class);
        intentToStartDetailActivity.putExtra("movieDetail", movieDetail);
        startActivity(intentToStartDetailActivity);
        */
    }

    @Override
    public void movieFinishedTask(ArrayList<Movie> movies) {
        if(movies != null) {
            showMovieDataView();
            mMovieAdapter.setMovies(movies);
        }else{
            showErrorMessage();
        }
    }

    public interface onSelectedMovieListener {
        void onSelectedMovie(Movie movie);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            this.movieListener = (onSelectedMovieListener) context;
        } catch (ClassCastException e){
            e.printStackTrace();
        }
    }
}
