package com.onemargaro.android.moviesfragments.tasks;

import android.content.Context;
import android.os.AsyncTask;


import com.onemargaro.android.moviesfragments.models.Review;
import com.onemargaro.android.moviesfragments.util.MovieJsonUtils;
import com.onemargaro.android.moviesfragments.util.NetworkUtils;

import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Margaro on 17/10/17.
 */

public class ReviewTask extends AsyncTask<String, Void, ArrayList<Review>> {

    private Context context;
    private ReviewTaskAsync reviewMainThread;

    public ReviewTask(ReviewTaskAsync reviewMainThread, Context context){
        this.context = context;
        this.reviewMainThread = reviewMainThread;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(ArrayList<Review> review) {
        reviewMainThread.reviewFinishedTask(review);
    }

    @Override
    protected ArrayList<Review> doInBackground(String... params) {
        if(params.length==0){
            return null;
        }

        String urlStr = params[0];
        URL reviewsUrl = NetworkUtils.buildUrl(urlStr);
        System.out.println(reviewsUrl.toString());

        try{
            String jsonReviewResponse = NetworkUtils
                    .getResponseFromHttpUrl(reviewsUrl);


            ArrayList<Review> simpleJsonReviews = MovieJsonUtils
                    .getSimpleReviewStringsFromJson(context, jsonReviewResponse);

            return simpleJsonReviews;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

}
