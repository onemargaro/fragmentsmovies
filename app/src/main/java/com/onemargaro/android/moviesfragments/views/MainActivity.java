package com.onemargaro.android.moviesfragments.views;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;


import com.onemargaro.android.moviesfragments.R;
import com.onemargaro.android.moviesfragments.models.Movie;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MovieListFragment.onSelectedMovieListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        this.createFragmentMain("top_rated");
    }

    private void createFragmentMain(String typeList) {
        Bundle args = new Bundle();
        args.putString("typelist", typeList);
        MovieListFragment mlf = MovieListFragment.newInstance(args);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_fragment, mlf)
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int menuItemThatWasSelected = item.getItemId();
        if(menuItemThatWasSelected == R.id.mi_order_by_most_popular){
            this.createFragmentMain("popular");
            return true;
        }
        if(menuItemThatWasSelected == R.id.mi_order_by_top_rated){
            this.createFragmentMain("top_rated");
            return true;
        }
        if(menuItemThatWasSelected == R.id.mi_latest){
            this.createFragmentMain("now_playing");
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onSelectedMovie(Movie movie) {
        System.out.println(movie.getOriginalTitle());
        Bundle args = new Bundle();
        args.putParcelable("movieDetail", movie);
        MovieDetailFragment mdf = MovieDetailFragment.newInstance(args);
        if (getResources().getBoolean(R.bool.two_fragments)){
            //nuevo
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.description_movie_fragment, mdf)
                    .commit();
        }else{
            //reemplazar
            getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack("main_fragment")
                    .replace(R.id.main_fragment, mdf)
                    .commit();
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().findFragmentByTag("main_fragment") != null) {
            getSupportFragmentManager().popBackStack("main_fragment",
                    getSupportFragmentManager().POP_BACK_STACK_INCLUSIVE);
        } else {
            super.onBackPressed();
        }
    }
}
