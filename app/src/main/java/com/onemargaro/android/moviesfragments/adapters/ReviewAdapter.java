package com.onemargaro.android.moviesfragments.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.onemargaro.android.moviesfragments.R;
import com.onemargaro.android.moviesfragments.models.Review;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Margaro on 17/10/17.
 */

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ReviewViewHolder> {

    /**
     * Manejar el evento de onclick
     */
    private final ReviewAdapterOnClickHandler rClickHandler;
    public List<Review> rReviewsData;

    /**
     * Se implementa el metodo de la interfaz
     */
    public ReviewAdapter(ReviewAdapterOnClickHandler rClickHandler) {
        this.rClickHandler = rClickHandler;
    }

    public interface ReviewAdapterOnClickHandler{
        void onClick(Review movieDetail);
    }

    @Override
    public ReviewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context =  parent.getContext();
        int layoutIdForListItem = R.layout.movie_item_review;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem, parent, shouldAttachToParentImmediately);
        return new ReviewViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ReviewViewHolder holder, int position) {
        Review review = rReviewsData.get(position);
        holder.tv_author.setText(review.getAuthor());
        holder.tv_review.setText(review.getContent());
    }

    @Override
    public int getItemCount() {
        if (null == rReviewsData) return 0;
        return  rReviewsData.size();
    }

    public class ReviewViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.tv_author)
        TextView tv_author;
        @BindView(R.id.tv_review)
        TextView tv_review;

        public ReviewViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int adapterPosition = getAdapterPosition();
            Review review = rReviewsData.get(adapterPosition);
            rClickHandler.onClick(review);
        }
    }

    public void setReviews(List reviewsData){
        rReviewsData= reviewsData;
        notifyDataSetChanged();
    }

    public ArrayList getMovies(){
        return   (ArrayList) rReviewsData;
    }
}
