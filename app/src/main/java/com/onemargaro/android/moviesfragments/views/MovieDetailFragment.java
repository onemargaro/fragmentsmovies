package com.onemargaro.android.moviesfragments.views;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.onemargaro.android.moviesfragments.R;
import com.onemargaro.android.moviesfragments.adapters.ReviewAdapter;
import com.onemargaro.android.moviesfragments.adapters.VideoAdapter;
import com.onemargaro.android.moviesfragments.data.MovieContract;
import com.onemargaro.android.moviesfragments.models.Movie;
import com.onemargaro.android.moviesfragments.models.Review;
import com.onemargaro.android.moviesfragments.models.Video;
import com.onemargaro.android.moviesfragments.tasks.ReviewTask;
import com.onemargaro.android.moviesfragments.tasks.ReviewTaskAsync;
import com.onemargaro.android.moviesfragments.tasks.VideoTask;
import com.onemargaro.android.moviesfragments.tasks.VideoTaskAsync;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MovieDetailFragment extends Fragment implements
        ReviewAdapter.ReviewAdapterOnClickHandler,
        ReviewTaskAsync,
        VideoAdapter.VideoAdapterOnClickHandler,
        VideoTaskAsync{
    @BindView(R.id.tv_original_title_display)
    TextView mOriginalTitle;
    @BindView(R.id.tv_overview_display)
    TextView mOverview;
    @BindView(R.id.tv_user_rating_display)
    TextView mRating;
    @BindView(R.id.tv_release_date_display)
    TextView mReleaseDate;
    @BindView(R.id.iv_movie_poster)
    ImageView mPoster;
    @BindView(R.id.rb_vote_average)
    RatingBar mVoteAverage;
    @BindView(R.id.title_movie)
    TextView mTitle;
    @BindView(R.id.fav_btn)
    Button fav_btn;
    @BindView(R.id.rcv_reviews)
    RecyclerView rcv_reviews;
    @BindView(R.id.rcv_trailers)
    RecyclerView rcv_trailers;
    private Movie movie;

    private GridLayoutManager reviewGridLayoutManager;
    private LinearLayoutManager videoLinearLayoutManager;
    private ReviewAdapter reviewAdapter;
    private VideoAdapter videoAdapter;

    public static MovieDetailFragment newInstance(Bundle args) {
        MovieDetailFragment mdf = new MovieDetailFragment();
        if (args != null)
            mdf.setArguments(args);
        return mdf;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Activity activity = getActivity();
        View rootView = inflater.inflate(R.layout.fragmet_movie_detail, container, false);

        ButterKnife.bind(this, rootView);

        if(getArguments().containsKey("movieDetail")){
            //Bundle extras = intent.getExtras();
            this.movie = getArguments().getParcelable("movieDetail");
            if(movie!=null){
                if(this.CheckDatabase(movie.id)) {
                    this.fav_btn.setText("No Favorito");
                }else{
                    this.fav_btn.setText("Favorito");
                }
                mOriginalTitle.setText(movie.originalTitle);
                mTitle.setText(movie.title);
                mOverview.setText(movie.overview);
                mRating.setText(movie.voteAverage.toString());
                String str = movie.voteAverage.toString();
                float rate = Float.parseFloat(str);
                mVoteAverage.setRating(rate);
                mReleaseDate.setText(movie.releaseDate);
                Context context =mPoster.getContext();
                String path ="http://image.tmdb.org/t/p/w780/"+movie.posterPath;
                Picasso.with(context).load(path)
                        .placeholder(R.drawable.moviefilm2)
                        .error(R.drawable.error_load2)
                        //.resize(300,400)
                        .into(mPoster);

                /**
                 * Aqui se pone el GridLayout de reviews
                 */
                int orientation = getResources().getConfiguration().orientation;
                if(orientation == Configuration.ORIENTATION_LANDSCAPE)
                    reviewGridLayoutManager = new GridLayoutManager(activity,3);
                else
                    reviewGridLayoutManager = new GridLayoutManager(activity,2);

                rcv_reviews.setLayoutManager(reviewGridLayoutManager);
                rcv_reviews.setHasFixedSize(true);

                reviewAdapter = new ReviewAdapter(this);
                rcv_reviews.setAdapter(reviewAdapter);

                new ReviewTask(this, activity).execute(movie.id+"/reviews");

                videoLinearLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
                rcv_trailers.setLayoutManager(videoLinearLayoutManager);
                rcv_trailers.setHasFixedSize(true);

                videoAdapter = new VideoAdapter(this);
                rcv_trailers.setAdapter(videoAdapter);
                new VideoTask(this, activity).execute(movie.id+"/videos");

            }
        }
        return rootView;
    }

    private Boolean CheckDatabase(Integer movieId) {
        Cursor cursor = getActivity().getContentResolver().query(
                MovieContract.MovieEntry.CONTENT_URI.buildUpon().appendPath(String.valueOf(movieId)).build(),
                null,
                null,
                null,
                MovieContract.MovieEntry.COLUMN_MOVIE_ID
        );
        if (cursor != null) return cursor.moveToFirst();
        return false;
    }
    /**
     * Aqui se asocia el
     * @param view
     */
    @OnClick(R.id.fav_btn)
    public void FavAction(View view) {
        String title = this.movie.getOriginalTitle();
        String overview = this.movie.getOverview();
        String posterPath = this.movie.getPosterPath();
        Integer idMovie = this.movie.getId();

        ContentValues contentValues = new ContentValues();
        contentValues.put(MovieContract.MovieEntry.COLUMN_MOVIE_ID,  idMovie);
        contentValues.put(MovieContract.MovieEntry.COLUMN_TITLE, title);
        contentValues.put(MovieContract.MovieEntry.COLUMN_OVERVIEW, overview);
        contentValues.put(MovieContract.MovieEntry.COLUMN_POSTER_PATH, posterPath);

        if (this.fav_btn.getText() == "Favorito") {
            Uri uri = getActivity().getContentResolver().insert(MovieContract.MovieEntry.CONTENT_URI, contentValues);
            if(uri != null){
                Toast.makeText(getActivity().getBaseContext(), uri.toString(), Toast.LENGTH_LONG).show();
            }
            this.fav_btn.setText("No Favorito");
        }else {
            int deleted = getActivity().getContentResolver().delete(
                    MovieContract.MovieEntry.CONTENT_URI.buildUpon().appendPath(String.valueOf(idMovie)).build(),
                    null,
                    null);
            if(deleted != 0){
                Toast.makeText(getActivity().getBaseContext(), "Se eliminó de Favoritos :C ", Toast.LENGTH_LONG).show();
            }
            this.fav_btn.setText("Favorito");
        }
    }


    @Override
    public void onClick(Review movieDetail) {
        Uri uri = Uri.parse(movieDetail.getUrl());
        startActivity(new Intent(Intent.ACTION_VIEW, uri));
    }

    @Override
    public void reviewFinishedTask(ArrayList<Review> reviews) {
        reviewAdapter.setReviews(reviews);
    }

    @Override
    public void videoFinishedTask(ArrayList<Video> video) {
        videoAdapter.setVideos(video);
    }

    @Override
    public void videoClick(Video video) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube://www.youtube.com/watch?v=" + video.getKey())));
    }
}
