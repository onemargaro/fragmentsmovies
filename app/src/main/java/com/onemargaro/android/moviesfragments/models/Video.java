package com.onemargaro.android.moviesfragments.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Margaro on 26/10/17.
 */

public class Video implements Parcelable {
    private String id;
    private String key;
    private String name;
    private String type;

    public Video(JSONObject json) throws JSONException {
        this.id = json.getString("id");
        this.key = json.getString("key");
        this.name = json.getString("name");
        this.type = json.getString("type");
    }

    public Video(Parcel p){
        this.id = p.readString();
        this.key = p.readString();
        this.name = p.readString();
        this.type = p.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.id);
        parcel.writeString(this.key);
        parcel.writeString(this.name);
        parcel.writeString(this.type);
    }

    public static final Parcelable.Creator<Video> CREATOR = new Parcelable.Creator<Video>(){

        @Override
        public Video createFromParcel(Parcel parcel) {
            return new Video(parcel);
        }

        @Override
        public Video[] newArray(int i) {
            return new Video[i];
        }
    };


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
