package com.onemargaro.android.moviesfragments.tasks;

import com.onemargaro.android.moviesfragments.models.Review;

import java.util.ArrayList;

/**
 * Created by Margaro on 17/10/17.
 */

public interface ReviewTaskAsync {
    void reviewFinishedTask(ArrayList<Review> reviews);
}
