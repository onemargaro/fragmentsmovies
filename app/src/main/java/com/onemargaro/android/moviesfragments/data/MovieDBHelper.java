package com.onemargaro.android.moviesfragments.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Margaro on 26/10/17.
 */

public class MovieDBHelper extends SQLiteOpenHelper {
    public static final int BASE_VERSION =  1;
    public static final String DATABASE_NAME = "movies.db";

    public MovieDBHelper(Context context) {
        super(context, DATABASE_NAME, null, BASE_VERSION);
    }

    private void createMovieTable(SQLiteDatabase db){
        String sql = "CREATE TABLE " + MovieContract.MovieEntry.TABLE_NAME + "(" +
                MovieContract.MovieEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
                MovieContract.MovieEntry.COLUMN_MOVIE_ID + " INTEGER NOT NULL, "+
                MovieContract.MovieEntry.COLUMN_TITLE + " TEXT NOT NULL, "+
                MovieContract.MovieEntry.COLUMN_OVERVIEW + " TEXT NOT NULL, " +
                MovieContract.MovieEntry.COLUMN_POSTER_PATH + " TEXT NOT NULL,"+
                "UNIQUE ("+ MovieContract.MovieEntry.COLUMN_MOVIE_ID + ")" +
                ");";
        db.execSQL(sql);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        this.createMovieTable(sqLiteDatabase);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
