package com.onemargaro.android.moviesfragments.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by Margaro on 26/10/17.
 */

public class MovieContentProvider extends ContentProvider {

    private MovieDBHelper mMovieDbHelper;

    public static final int MOVIES = 100;
    public static final int MOVIES_WITH_ID = 101;



    private static final UriMatcher sUriMatcher = buildUriMatcher();

    public static UriMatcher buildUriMatcher(){
        UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

        uriMatcher.addURI(MovieContract.AUTHORITY, MovieContract.PATH_MOVIES, MOVIES);
        uriMatcher.addURI(MovieContract.AUTHORITY, MovieContract.PATH_MOVIES + "/#", MOVIES_WITH_ID);

        return uriMatcher;
    }

    @Override
    public boolean onCreate() {
        Context context = getContext();
        mMovieDbHelper = new MovieDBHelper(context);
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        final SQLiteDatabase db = mMovieDbHelper.getReadableDatabase();
        int match = sUriMatcher.match(uri);

        Cursor retCursor;
        switch (match){
            case MOVIES:
                retCursor = db.query(MovieContract.MovieEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;

            case MOVIES_WITH_ID:
                String id = uri.getPathSegments().get(1);
                Log.i(MovieContentProvider.class.getSimpleName(), "el id de la pelicula es: "+id);

                String mSelection = MovieContract.MovieEntry.COLUMN_MOVIE_ID+"=?";
                String[] mSelectionArgs = new String[]{id};
                retCursor = db.query(MovieContract.MovieEntry.TABLE_NAME,
                        projection,
                        mSelection,
                        mSelectionArgs,
                        null,
                        null,
                        sortOrder);
                break;

            default:
                throw  new UnsupportedOperationException("Unknown uri: "+uri);
        }

        retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return  retCursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        final SQLiteDatabase db = mMovieDbHelper.getWritableDatabase();
        int match = sUriMatcher.match(uri);
        Uri returnUri;
        switch (match){
            case MOVIES:
                long id = db.insert(MovieContract.MovieEntry.TABLE_NAME, null, values);
                if(id > 0){
                    returnUri = ContentUris.withAppendedId(MovieContract.MovieEntry.CONTENT_URI, id);
                }else{
                    Integer idMovie  = values.getAsInteger(MovieContract.MovieEntry.COLUMN_MOVIE_ID);
                    String idMov = idMovie.toString();
                    Cursor cursor = db.query(MovieContract.MovieEntry.TABLE_NAME,null, MovieContract.MovieEntry.COLUMN_MOVIE_ID+"=?",new String[]{idMovie.toString()},
                            null, null, null, null);
                    if(cursor!= null){
                        returnUri = ContentUris.withAppendedId(MovieContract.MovieEntry.CONTENT_URI, idMovie);
                    }else{
                        throw new android.database.SQLException("Failed to inser row into "+ uri);
                    }
                }
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: "+uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        final SQLiteDatabase db = mMovieDbHelper.getWritableDatabase();

        int match = sUriMatcher.match(uri);

        int movieDeleted;

        switch (match){
            case MOVIES_WITH_ID:
                String id = uri.getPathSegments().get(1);
                movieDeleted = db.delete(MovieContract.MovieEntry.TABLE_NAME, "movieid=?",new String[]{id});
                break;
            default:
                throw new UnsupportedOperationException("UNKNOWN URI: "+uri);
        }

        if(movieDeleted != 0){
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return  movieDeleted;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        final SQLiteDatabase db = mMovieDbHelper.getWritableDatabase();

        int movieUpdated;
        int match = sUriMatcher.match(uri);
        switch (match){
            case MOVIES_WITH_ID:
                String id = uri.getPathSegments().get(1);
                movieUpdated = db.update(MovieContract.MovieEntry.TABLE_NAME, values, MovieContract.MovieEntry.COLUMN_MOVIE_ID+"=?",
                        new String[]{id});
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: "+uri);
        }

        if(movieUpdated != 0){
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return movieUpdated;
    }
}