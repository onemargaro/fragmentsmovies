package com.onemargaro.android.moviesfragments.tasks;

import com.onemargaro.android.moviesfragments.models.Movie;

import java.util.ArrayList;

/**
 * Created by Adrian Ulises Mercado Martínez on 26/05/17.
 *
 * This code is based on student and sunshine examples
 * of the course Associate Android Developer Fast Track Nanodegree Program of Udacity
 */

public interface MovieTaskAsync {
    void movieFinishedTask(ArrayList<Movie> movies);
}
