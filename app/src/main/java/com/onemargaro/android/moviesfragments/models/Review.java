package com.onemargaro.android.moviesfragments.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Margaro on 26/10/17.
 */

public class Review implements Parcelable {
    private String id;
    private String author;
    private String content;
    private String url;

    /**
     *
     * @param jsonObject this the object to transform in a Review
     * @throws JSONException
     */
    public Review(JSONObject jsonObject)throws JSONException {
        this.id = jsonObject.getString("id");;
        this.author = jsonObject.getString("author");
        this.content = jsonObject.getString("content");
        this.url = jsonObject.getString("url");
    }


    public Review(Parcel p) {
        this.id = p.readString();
        this.author = p.readString();
        this.content = p.readString();
        this.url = p.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(author);
        parcel.writeString(content);
        parcel.writeString(url);
    }

    public static final Parcelable.Creator<Review> CREATOR = new Parcelable.Creator<Review>(){
        @Override
        public Review createFromParcel(Parcel source) {
            return new Review(source);
        }

        @Override
        public Review[] newArray(int size) {
            return new Review[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
