package com.onemargaro.android.moviesfragments.util;

import android.content.Context;

import com.onemargaro.android.moviesfragments.models.Movie;
import com.onemargaro.android.moviesfragments.models.Review;
import com.onemargaro.android.moviesfragments.models.Video;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;

/**
 * Created by Adrian Ulises Mercado Martínez on 26/05/17.
 *
 * This code is based on student and sunshine examples
 * of the course Associate Android Developer Fast Track Nanodegree Program of Udacity
 */

public class MovieJsonUtils {

    public static ArrayList<Movie> getSimpleMovieStringsFromJson(Context context, String movieJsonStr)throws JSONException {
        final String STATUS_CODE = "status_code";
        final String RESULT ="results";
        ArrayList parseMovieData = new ArrayList();
        JSONObject movieJson = new JSONObject(movieJsonStr);

        if(movieJson.has(STATUS_CODE)){
            int errorCode = movieJson.getInt(STATUS_CODE);

            switch (errorCode){
                case HttpURLConnection.HTTP_NOT_FOUND:
                case HttpURLConnection.HTTP_NOT_AUTHORITATIVE:
                    return null;
                default:
                    break;
            }
        }

        JSONArray movieArray = movieJson.getJSONArray(RESULT);
        for(int i = 0; i <movieArray.length();i++){
            parseMovieData.add(new Movie(movieArray.getJSONObject(i)));
        }

        return parseMovieData;
    }

    public static ArrayList<Review> getSimpleReviewStringsFromJson(Context context, String reviewJsonStr)throws JSONException {
        final String STATUS_CODE = "status_code";
        final String RESULT ="results";
        ArrayList parseReviewData = new ArrayList();
        JSONObject reviewJson = new JSONObject(reviewJsonStr);

        if(reviewJson.has(STATUS_CODE)){
            int errorCode = reviewJson.getInt(STATUS_CODE);

            switch (errorCode){
                case HttpURLConnection.HTTP_NOT_FOUND:
                case HttpURLConnection.HTTP_NOT_AUTHORITATIVE:
                    return null;
                default:
                    break;
            }
        }

        JSONArray reviewArray = reviewJson.getJSONArray(RESULT);
        for(int i = 0; i <reviewArray.length();i++){
            parseReviewData.add(new Review(reviewArray.getJSONObject(i)));
        }
        return parseReviewData;
    }

    public static ArrayList<Video> getSimpleVideosStringsFromJson(Context context, String videosJsonStr)throws JSONException {
        final String STATUS_CODE = "status_code";
        final String RESULT ="results";
        ArrayList parseVideoData = new ArrayList();
        JSONObject videosJson = new JSONObject(videosJsonStr);

        if(videosJson.has(STATUS_CODE)){
            int errorCode = videosJson.getInt(STATUS_CODE);

            switch (errorCode){
                case HttpURLConnection.HTTP_NOT_FOUND:
                case HttpURLConnection.HTTP_NOT_AUTHORITATIVE:
                    return null;
                default:
                    break;
            }
        }

        JSONArray videosArray = videosJson.getJSONArray(RESULT);
        for(int i = 0; i <videosArray.length();i++){
            parseVideoData.add(new Video(videosArray.getJSONObject(i)));
        }
        return parseVideoData;
    }

}
