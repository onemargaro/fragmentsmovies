package com.onemargaro.android.moviesfragments.data;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by Margaro on 26/10/17.
 */

public class MovieContract {
    public static final String AUTHORITY = "com.onemargaro.android.moviesfragments";
    public static final String PATH_MOVIES = "movies";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://"+AUTHORITY);

    public static class MovieEntry implements BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_MOVIES).build();

        public static final String TABLE_NAME = "movies";

        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_MOVIE_ID = "movieId";
        public static final String COLUMN_OVERVIEW = "overview";
        public static final String COLUMN_POSTER_PATH = "poster_path";
    }

}
